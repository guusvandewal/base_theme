<?php include_once('header.inc'); ?>

<div class="container clearfix typography">
  <div class="row">
    <div class="main-content">
      <?php if ($messages): ?>
        <div class="container">
          <div class="row">
            <div id="messages">
              <?php print $messages; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <?php if (isset($is_panelized) && $is_panelized): ?>
        <?php print render($page['content']); ?>
      <?php else: ?>

        <?php if (isset($title)): ?>
          <h1 class="full-node-title"><?php print check_plain($title); ?></h1>
        <?php endif; ?>

        <?php print render($page['content']); ?>

      <?php endif; ?>
    </div>
  </div>
</div>
<?php include_once('footer.inc'); ?>

