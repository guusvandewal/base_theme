<?php
  $language = '';
  if(isset($content['field_language'])) {
    $language = $content['field_language']['#items'][0]['safe_value'];
  }
  else {
    $language = 'markup';
  }
 ?>
<pre class="line-numbers"><code class="language-<?php print $language ?>"><?php print render($content); ?></code></pre>
