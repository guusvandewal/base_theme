<?php

function guusvandewal_id_safe($string) {
  if (is_numeric($string{0})) {
    // if the first character is numeric, add 'n' in front
    $string = 'n' . $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-?]+/', '-', $string));
}

// ---------------------------------------------------------------------------
// Remove redundant CSS
function guusvandewal_css_alter(&$css) {
  //make a list of module css to remove
  $css_to_remove = array();
  //$css_to_remove[] = drupal_get_path('module','system').'/system.base.css';
  $css_to_remove[] = drupal_get_path('module', 'system') . '/system.menus.css';
  $css_to_remove[] = drupal_get_path('module', 'system') . '/system.messages.css';
  $css_to_remove[] = drupal_get_path('module', 'system') . '/system.theme.css';
  $css_to_remove[] = drupal_get_path('module', 'user') . '/user.css';
  $css_to_remove[] = drupal_get_path('module', 'field') . '/theme/field.css)';
  $css_to_remove[] = drupal_get_path('module', 'ctools') . '/css/ctools.css)';

  $css_to_remove[] = drupal_get_path('module', 'node') . '/node.css';
  $css_to_remove[] = drupal_get_path('module', 'views') . '/css/views.css';
  $css_to_remove[] = drupal_get_path('module', 'search') . '/search.css';
  $css_to_remove[] = drupal_get_path('module', 'webform') . '/css/webform.css';
  $css_to_remove[] = drupal_get_path('module', 'field_group') . '/horizontal-tabs/horizontal-tabs.css';
  $css_to_remove[] = drupal_get_path('module', 'date') . '/date_api/date.css';
  // now we can remove the contribs from the array
  foreach ($css_to_remove as $index => $css_file) {
    if(isset($css[$css_file])) {
      unset($css[$css_file]);
    }
  }
}


function guusvandewal_preprocess_html(&$vars) {
  //normal stylesheets
  drupal_add_css(drupal_get_path('theme', 'guusvandewal') . '/assets/css/rendered/screen.css', array(
    'group' => CSS_THEME,
    'browsers' => array(
      'IE' => FALSE,
      ),
    )
  );
  //Stylesheets for IE
  drupal_add_css(drupal_get_path('theme', 'guusvandewal') . '/assets/css/rendered/ie.css', array(
    'group' => CSS_THEME,
    'browsers' => array(
      'IE' => 'lte IE 9',
      '!IE' => FALSE
    ),
    'preprocess' => FALSE
    )
  );

  drupal_add_js(drupal_get_path('theme', 'guusvandewal') . '/assets/js/modernizr.js');
  drupal_add_js(drupal_get_path('theme', 'guusvandewal') . '/assets/js/jquery.tabSlideOut.v1.3.js');
  drupal_add_js(drupal_get_path('theme', 'guusvandewal') . '/assets/js/edit_sidebar.js');
  drupal_add_js(drupal_get_path('theme', 'guusvandewal') . '/assets/js/respond.min.js');

  drupal_add_css(drupal_get_path('theme', 'guusvandewal') . '/assets/js/prism/prism.css');
  drupal_add_js(drupal_get_path('theme', 'guusvandewal') . '/assets/js/prism/min/prism-ck.js');
  //drupal_add_js(drupal_get_path('theme', 'guusvandewal') . '/assets/js/script.js');

}

/*
 * Implements hook_html_head_alter
 */
function guusvandewal_html_head_alter(&$head_elements) {
  // Force the latest IE rendering engine and Google Chrome Frame.
  $head_elements['chrome_frame'] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array('http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1'),
  );
  $head_elements['viewport'] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array('meta name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, user-scalable=no'),
  );
}

function guusvandewal_preprocess_page(&$vars, $hook) {

  if(isset($vars['node']->title)) {


    $element = array(
      '#tag' => 'meta', // The #tag is the html tag - <link />
      '#attributes' => array( // Set up an array of attributes inside the tag
        'property' => 'og:title',
        'content' => $vars['node']->title,
      ),
    );
    drupal_add_html_head($element, 'og_title');
  }

  $status = drupal_get_http_header("status");


  if (isset($vars['node'])) {
    $vars['theme_hook_suggestions'][] = 'page__' . str_replace('_', '__', $vars['node']->type);
  }

  if ($status == "404 Not Found") {
    $vars['theme_hook_suggestions'][] = 'page__404';
  }
  if ($status == "403 Forbidden") {
    $vars['theme_hook_suggestions'][] = 'page__403';
  }



}

/**
 * Override or insert variables into the node template.
 */
function guusvandewal_preprocess_node(&$variables) {

  $variables['theme_hook_suggestions'][] = 'node__' . $variables['node']->type . '__' . $variables['view_mode'];
  $variables['classes_array'][] = $variables['view_mode'];
}

/*
 * Implementation of hook_menu_local_tasks().
 */
function guusvandewal_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul id="tab" class="nav nav-tabs">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

function guusvandewal_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('« first')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('‹ previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next ›')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last »')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . '<div class="pagination">' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('')),
    )). '</div>';
  }
}


function guusvandewal_pager_link($variables) {
  if(isset($variables['parameters']['search']) && $variables['parameters']['search'] == '') {
    unset($variables['parameters']['search']);
  }
  if(isset($variables['parameters']['sort_by']) && $variables['parameters']['sort_by'] == '') {
    unset($variables['parameters']['sort_by']);
  }
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];

  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }



  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

  // @todo l() cannot be used here, since it adds an 'active' class based on the
  //   path only (which is always the current path for pager links). Apparently,
  //   none of the pager links is active at any time - but it should still be
  //   possible to use l() here.
  // @see http://drupal.org/node/1410574



  $options = array('query' => $query);
  if(isset($parameters['fragment'])) {
    $options['fragment'] = $parameters['fragment'];
    unset($parameters['fragment']);
    unset($options['query']['fragment']);
  }


  $attributes['href'] = url($_GET['q'], $options);
  return '<a' . drupal_attributes($attributes) . '>' . check_plain($text) . '</a>';
}


/**
 * Implements HOOK_theme().
 */
function guusvandewal_theme(){
  return array(
    'nomarkup' => array (
      'render element' => 'element',
    ),
  );
}

## the nomarkup theme function
function theme_nomarkup($variables) {
  $output = '';
  // Render the items.
  foreach ($variables['items'] as $delta => $item) {
    $output .=  drupal_render($item);
  }
  return $output;
}

/* FORMS IN BOOTSTRAP */



function guusvandewal_textarea($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
  _form_set_class($element, array('form-textarea'));

  $wrapper_attributes = array(
    'class' => array('form-textarea-wrapper'),
  );

  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $wrapper_attributes['class'][] = 'resizable';
  }

  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';
  return $output;
}


function guusvandewal_form_element($variables) {
  $element = &$variables['element'];
  $output = '';

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');

    //Add Class radio to radio wrapper
    if($element['#type'] == 'radio') {
      $attributes['class'][] = 'radio';
      $attributes['class'][] = 'inline';
    }
    //Add Class checkbox to checkbox wrapper // Add class inline to checkbox
    elseif($element['#type'] == 'checkbox') {
      $attributes['class'][] = 'checkbox';
    }
    else {
      $attributes['class'][] = 'control-group';
    }
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  if(isset($element['#parents']) && form_get_error($element)) {
    $attributes['class'][] = 'error';
  }

  /* Not for radio or Checkbox alas*/
  if(($element['#type'] == 'radio') || ($element['#type'] == 'checkbox')):
    $output .= '<div' . drupal_attributes($attributes) . '>' . "\n";

    // If #title is not set, we don't display any label or required marker.
    if (!isset($element['#title'])) {
      $element['#title_display'] = 'none';
    }
    $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
    $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

    switch ($element['#title_display']) {
      case 'before':
      case 'invisible':
      case 'after':
      case 'attribute':
      case 'none':
      case 'inline' :

        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        $output .= ' ' . theme('form_element_label', $variables);
        if (!empty($element['#description'])) {
          $output .= ' <span class="help-block">' . $element['#description'] . "</span>";
        }

        break;
    }
    $output .= "</div>\n";

  else:

    $output .= '<div' . drupal_attributes($attributes) . '>' . "\n";

    // If #title is not set, we don't display any label or required marker.
    if (!isset($element['#title'])) {
      $element['#title_display'] = 'none';
    }
    $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
    $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

    switch ($element['#title_display']) {
      case 'before':
      case 'invisible':
      case 'after':
      case 'attribute':
      case 'none':
      case 'inline' :

        $output .= ' ' . theme('form_element_label', $variables);
        $output .= '<div class="controls">';
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        if (!empty($element['#description'])) {
          $output .= ' <span class="help-block">' . $element['#description'] . "</span>";
        }
        $output .= '</div>';
        break;
    }
    $output .= "</div>\n";
  endif;
  return $output;
}

function guusvandewal_webform_element($variables) {
  $element = &$variables['element'];
  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
    if($element['#type'] != 'radio') {
      $attributes['class'][] = 'control-group';
    }
  }

  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  if(isset($element['#parents']) && form_get_error($element)) {
    $attributes['class'][] = 'error';
  }

  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
    case 'inline' :

      $output .= ' ' . theme('form_element_label', $variables);
      $output .= '<div class="controls">';
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      if (!empty($element['#description'])) {
        $output .= ' <span class="help-block">' . $element['#description'] . "</span>";
      }
      $output .= '</div>';
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      if($element['#type'] == "markup") {
        $output .= ' ' . $prefix . $element['#webform_component']['value'] . $suffix . "\n";
      }else {
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      }
      break;
  }

  $output .= "</div>\n";
  return $output;
}
/*
 * Theme all form labels
 */
function guusvandewal_form_element_label($variables) {
  $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();

  // Style the label as class option to display inline with the element.

  if($element['#type'] == 'radio' || $element['#type'] == 'checkbox') {
    $extra_class = ' ';
  }
  else {
    $extra_class = ' control-label';
  }
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option' . $extra_class ;
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible' . $extra_class ;
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
    $attributes['class'] = $extra_class;
  }
  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}


function guusvandewal_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'btn form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'btn form-button-disabled';
  }

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}



/*
 * Theme Date Popup
 */

function guusvandewal_date_popup($vars) {

  $element = $vars['element'];
  $attributes = !empty($element['#wrapper_attributes']) ? $element['#wrapper_attributes'] : array('class' => array());
  $attributes['class'][] = 'container-inline-date clearfix';
  // If there is no description, the floating date elements need some extra padding below them.
  $wrapper_attributes = array('class' => array('date-padding'));
  if (empty($element['date']['#description'])) {
    $wrapper_attributes['class'][] = 'no-clearfix';
  }

  return '<div ' . drupal_attributes($attributes) .'>' . theme('form_element', $element) . '</div>';
}

/*
 * Theme Status Messages
 */

function guusvandewal_status_messages($variables) {

  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {

    $output .= "<div class=\"alert-block alert alert-$type\">\n";

    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n";
  }
  return $output;
}

//Add Table class to all generated tables
function guusvandewal_preprocess_table(&$variables) {
  $variables['attributes']['class'] = array('table', 'table-striped');
}


/**
 * Remove paragraph wrappers
 */
function guusvandewal_paragraphs_view($variables) {
  $element = $variables['element'];
  return  $element['#children'];
}
