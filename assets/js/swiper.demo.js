(function($){
  $(document).ready(function() {
    var config = {};
    config = {
      mode:'horizontal',
      updateOnImagesReady: true,
      calculateHeight: true,
      slidesPerView: 6,
      slidesPerGroup: 6,
      loop: true
    };
    var bottom_swiper = $('#partner_slider').swiper(config);
    $('.bottom_slider .partner_slider_prev').click(function() {
      bottom_swiper.swipePrev();
    });

    $('.bottom_slider .partner_slider_next').click(function() {
      bottom_swiper.swipeNext();
    });


    var configTop = {};
    configTop = {
      mode:'horizontal',
      updateOnImagesReady: true,
      calculateHeight: true,
      loop: true
    };
    var swiper = $('#frontpage-slider').swiper(configTop);

    var config = {};
    config = {
      mode:'horizontal',
      updateOnImagesReady: true,
      calculateHeight: true,
      loop: true
    };
    var logo_swiper = $('#logo-slider').swiper(config);
    $('#logo-slider .logo-slider_prev').click(function() {
      logo_swiper.swipePrev();
    });

    $('#logo-slider .logo-slider_next').click(function() {
      logo_swiper.swipeNext();
    });
  });
})(jQuery);
