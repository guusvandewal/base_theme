(function ($) {

  //Toggle grid for vertical rhythm
  $(window).keypress(function(event) {
    if (!(event.which == 7 && event.ctrlKey)) return true;
    $('body').toggleClass('grid-on');
    return false;
  });

  jQuery.extend( jQuery.fn, {
    // Name of our method & one argument (the parent selector)
    hasParent: function( p ) {
      // Returns a subset of items using jQuery.filter
      return this.filter(function () {
        // Return truthy/falsey based on presence in parent
        return $(p).find(this).length;
      });
    }
  });

  //Sidebar Menu
  var transform_prop = window.Modernizr.prefixed('transform'),
  transition_prop = window.Modernizr.prefixed('transition'),
  transition_end = (function() {
    var props = {
      'WebkitTransition' : 'webkitTransitionEnd',
      'MozTransition'    : 'transitionend',
      'OTransition'      : 'oTransitionEnd otransitionend',
      'msTransition'     : 'MSTransitionEnd',
      'transition'       : 'transitionend'
    };

    return props.hasOwnProperty(transition_prop) ? props[transition_prop] : false;
  })();

  var inner = $('#inner-wrap')[0];
  var nav_open = false;
  var nav_class = 'js-nav';

  function closeNavEnd(e) {
    if (e && e.target === inner) {

      document.removeEventListener(transition_end, closeNavEnd, false);
    }
    nav_open = false;
  }

  function closeNav() {
    if (nav_open) {
      // close navigation after transition or immediately
      var duration = (transition_end && transition_prop) ? parseFloat(window.getComputedStyle(inner, '')[transition_prop + 'Duration']) : 0;
      if (duration > 0) {
        document.addEventListener(transition_end, closeNavEnd, false);
      } else {
        closeNavEnd(null);
      }
    }
    $('html').removeClass(nav_class);
  }

  function openNav() {
    if (nav_open) {
      return;
    }
    $('html').addClass(nav_class);
    $('html').addClass('js-nav');
    nav_open = true;
  }

  function toggleNav(e) {

    if (nav_open && $('html').hasClass(nav_class)) {
      closeNav();
    } else {
      openNav();
    }
    if (e) {
      e.preventDefault();
    }
  }

  $('.toggle-menu').on('click', toggleNav);
  //$('#nav-closing-btn').click(toggleNav);
  $(document).click(function(e) {
    if (nav_open && !$(e.target).hasParent('nav')) {
      closeNav();
      e.preventDefault();
    }
  });

  $('html').addClass('js-ready');

  //100% height wrappers for menu sidebar
  function setElementHeight(el) {
    var windowHeight = $(window).height();
    if($(el).height() < $(window).height()) {
      $(el).height(windowHeight);
    }
  }

  if($('#outer-wrap').length > 0) {
    //setElementHeight($('#outer-wrap'));
    //setElementHeight($('#inner-wrap'));
  }

  window.onresize = function() {
    //setElementHeight($('#outer-wrap'));
    //setElementHeight($('#inner-wrap'));
  };

  //Submenu sidebar

    /*
     * only toggles when the link is clicked, not when the li is clicked
     * the active classes, however, are appended to the parent li of the link
     * so when you click the link, the parent li gets the classes
     * this fixes the issue of the submenu closing when clicking the submenu
     */

    var allSubs = $('li.has-submenu > ul').hide();
    var activeSubs = $('a.has-submenu');
    if ($('#nav').length > 0) {
        $('#nav').on('click', 'a.has-submenu', function() {
            if(!$(this).parent().hasClass('active-ul')) {
                activeSubs.parent().removeClass('active-ul');
                $(this).parent().addClass('active-ul');
                allSubs.slideUp();
                $(this).parent().find($('ul')).slideDown();
            } else { //check links
                allSubs.slideUp();
                activeSubs.parent().removeClass('active-ul');
            }

            return false;
        });
    }
  $('.hamburger-menu').bind('click', function() {
    $('html').toggleClass('main-menu')
  });

})(jQuery);
