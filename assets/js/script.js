(function ($) {
  Drupal.behaviors.guusvandewal = {

    attach: function(context, settings) {
      //your code here
      //$('pre code').each(function(i, e) {hljs.highlightBlock(e)});
      $(window).keypress(function(event) {
        if (!(event.which == 7 && event.ctrlKey)) return true;
        $('body').toggleClass('grid-on');
        return false;
      });
    }
  }
})(jQuery);
